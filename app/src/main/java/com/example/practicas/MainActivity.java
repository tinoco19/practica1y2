package com.example.practicas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button buttonlogin,buttonguardar,buttonbuscar,button_pasar_parametro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonlogin=findViewById(R.id.buttonlogin);
        buttonguardar=findViewById(R.id.buttonguardar);
        buttonbuscar=findViewById(R.id.buttonbuscar);
        button_pasar_parametro=findViewById(R.id.button_pasar_parametro);
        button_pasar_parametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(
                        MainActivity.this, practica2.class);
                startActivity(intent);
            }
        });
        buttonbuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(
                        MainActivity.this, buscar.class);
                startActivity(intent);
            }
        });
        buttonguardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(
                        MainActivity.this, guardar.class);
                startActivity(intent);
            }
        });

        buttonlogin.setOnClickListener( new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent intent=new Intent(
                        MainActivity.this, login.class);
                startActivity(intent);

            }
        });


    }

}

