package com.example.practicas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class practica2 extends AppCompatActivity {
    EditText escribir_parametro;
    Button pasar_parametro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practica2);
        escribir_parametro=findViewById(R.id.escribir_parametro);
        pasar_parametro=findViewById(R.id.pasar_parametro);
        pasar_parametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(
                        practica2.this, recibir_parametro.class);
                Bundle bundle=new Bundle();
                bundle.putString("dato",escribir_parametro.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }


}
