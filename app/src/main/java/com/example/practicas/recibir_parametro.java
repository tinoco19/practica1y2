package com.example.practicas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class recibir_parametro extends AppCompatActivity {
    TextView escribir_parametro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_parametro);

        escribir_parametro= findViewById(R.id.recibir_parametro);
        Bundle bundle=this.getIntent().getExtras();
        escribir_parametro.setText(bundle.getString("dato"));

    }


}
